<?php

namespace Whaai\WhaaiApi\Console;

use DB;
use Illuminate\Console\Command;

class ResetDatabase extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'reset:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset database and run all migrations';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info("Dropping database");
            DB::statement('DROP DATABASE IF EXISTS ' . \Config::get('database.connections.mysql.database'));
            $this->info("Creating database");
            DB::statement('CREATE DATABASE ' . \Config::get('database.connections.mysql.database'));
        } catch (\Exception $e) {

        }
        $this->info("Reconnect to the database");
        DB::reconnect();
        $this->info("Running migrations");
        $this->call('migrate', ['--seed' => 1]);
    }
}
