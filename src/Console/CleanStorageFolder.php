<?php

namespace Whaai\WhaaiApi\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use SplFileInfo;

class CleanStorageFolder extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'whaai_api:clean_storage_folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean the storage folder';

    public function handle()
    {
        $this->deleteFiles(storage_path('logs'));
        $this->deleteFiles(storage_path('debugbar'));
        $this->deleteFiles(storage_path('app'));
        $this->deleteFiles(storage_path('framework/databases'));
        $this->deleteFiles(public_path('public-temp'));
    }

    protected function deleteFiles($directory)
    {
        $files = glob($directory . "/*"); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file)) {
                $this->removeFile($file);
            } elseif (is_dir($file) && $file != $directory) {
                $this->deleteFiles($file);
                @rmdir($file);
            }
        }
    }

    public function removeFile($file)
    {
        $info = new SplFileInfo($file);
        if ($this->canDelete($info)) {
            @unlink($file);
        } else {
            $this->info($file);
        }
    }

    protected function canDelete(SplFileInfo $info)
    {
        //Dont remove git ignore files
        if (in_array($info->getExtension(), ['gitignore'])) {
            return false;
        }

        $created = Carbon::createFromTimestamp($info->getMTime());
        $today = new Carbon();

        //dont remove recent files
        if ($created->diffInHours($today) <= 1) {
            return false;
        }

        return true;
    }
}
