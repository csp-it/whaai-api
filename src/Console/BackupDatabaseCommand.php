<?php

namespace Whaai\WhaaiApi\Console;

use Whaai\WhaaiApi\Helpers\PathManager;
use Carbon\Carbon;
use HipChat\HipChat;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Illuminate\Filesystem\FilesystemManager;

class BackupDatabaseCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'whaai_api:backup_database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a backup for the database';

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;
    protected $cache_directory;
    protected $zip_file;

    /**
     * @var DatabaseManager
     */
    protected $database_connection;

    /**
     * BackupDatabaseCommand constructor.
     * @param FilesystemManager $manager
     * @param DatabaseManager $databaseManager
     */
    public function __construct(FilesystemManager $manager, DatabaseManager $databaseManager)
    {
        parent::__construct();
        $this->database_connection = $databaseManager->connection();
        $this->filesystem = $manager->disk(config('whaai-api.backup.disk'));
        $this->zip_file = Carbon::now()->format("Y-m-d_Hi") . '.zip';
    }

    public function handle()
    {
        if (config('whaai-api.backup.enabled')) {
            $this->info("Starting backup of the database");
            $this->cache_directory = (new PathManager())->getPrivatePath() . '/';
            $this->dumpDatabases();
            $this->uploadZipFile();
            $this->notifyHipChat();
        }
    }

    protected function dumpDatabases()
    {
        $mysqlDump = config('whaai-api.backup.mysqldump');
        //Run command to export main data excluding logs table if the option is set.
        $command = sprintf("%s --extended-insert --complete-insert --add-drop-table --add-drop-database --user=%s --password='%s' --host=%s %s > %s.sql",
            $mysqlDump,
            $this->database_connection->getConfig('username'),
            $this->database_connection->getConfig('password'),
            $this->database_connection->getConfig('host'),
            $this->database_connection->getDatabaseName(),
            $this->cache_directory . $this->database_connection->getDatabaseName()
        );

        exec($command);

        zip($this->cache_directory, $this->cache_directory . $this->zip_file);
    }

    protected function uploadZipFile()
    {
        $this->info("Uploading file to backup server");
        $pathToZip = $this->cache_directory . $this->zip_file;
        $fullPath = "/websites/" . preg_replace("(^https?://)", "", config("app.url")) . '/' . date('Y-m') . '/';
        $this->filesystem->makeDirectory($fullPath);
        $this->filesystem->put($fullPath . $this->zip_file, fopen($pathToZip, 'r+'));
        $this->info("Upload completed");
    }

    protected function notifyHipChat()
    {
        if (config('whaai-api.backup.hipchat_notification')) {
            $html = '<strong>WEBSITE DATABASE BACKUP COMPLETE</strong>
                    <br>
                    Size: <strong>' . bytesOnHumanReadable(filesize($this->cache_directory . $this->zip_file)) . '</strong> -
                    Environment: <strong>' . app()->environment() . '</strong> -
                    Website: <strong>' . config('app.url') . '</strong> -
                    File name: <strong>' . $this->zip_file . '</strong>';

            $hipchat = new HipChat(config('whaai-api.backup.hipchat_api_token'));
            $hipchat->message_room(config('whaai-api.backup.hipchat_room'), 'API', $html, true, HipChat::COLOR_PURPLE);
        }
    }
}