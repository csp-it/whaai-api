<?php

namespace Whaai\WhaaiApi\Console;

use Whaai\WhaaiApi\Api\Models\ApiRequestCache;
use Whaai\WhaaiApi\Api\Models\FileCache;
use Whaai\WhaaiApi\Api\Wrappers\Files\Files;
use Illuminate\Console\Command;

class UpdateFilesCache extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'whaai_api:update_files_cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the files cache for the whaai API';

    public function handle(Files $files)
    {
        $this->info("Updating files API cache");
        $this->call("cache:clear");
        $itemTypes = config('whaai-api.files.synced_item_types');
        $files = $files->where('item_type', implode(',', $itemTypes))->all();

        foreach ($files as $file) {
            $this->info("Saving file: ".$file->id.' in cache');
            $file->saveInCache();
        }

        $this->info("Files saved in cache");
    }
}