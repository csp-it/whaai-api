<?php

namespace Whaai\WhaaiApi\Console;

use Whaai\WhaaiApi\Api\Models\ApiRequestCache;
use Whaai\WhaaiApi\Api\Wrappers\Files\Files;
use Whaai\WhaaiApi\ApiEndpointsForCache;
use Illuminate\Console\Command;

class UpdateApiCache extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'whaai_api:update_api_cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the api cache for the whaai API';

    public function handle(Files $files)
    {
        $class = config('whaai-api.api_cache_class', null);
        ApiRequestCache::truncate();
        $this->info("Cleaning cache");
        $this->call('cache:clear');
        if (class_exists($class)) {
            $this->info("Updating API cache");
            /** @var ApiEndpointsForCache $registrar */
            $registrar = new $class;
            $registrar->updateCache();
            $this->info("API saved in cache");
        }
    }
}