<?php

namespace Whaai\WhaaiApi;

use Whaai\WhaaiApi\Api\Collections\Addresses;
use Whaai\WhaaiApi\Api\Wrappers\Cms\CmsWebsites;
use Whaai\WhaaiApi\Api\Wrappers\Contacts\ContactCategories;
use Whaai\WhaaiApi\Api\Wrappers\Contacts\Contacts;
use Whaai\WhaaiApi\Api\Wrappers\Countries;
use Whaai\WhaaiApi\Api\Wrappers\Files\Files;
use Whaai\WhaaiApi\Api\Wrappers\Identifications;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryCategories;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryItems;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryOptions;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryPrices;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryPurchaseOrders;
use Whaai\WhaaiApi\Api\Wrappers\PaymentGateways\PaymentMethods;
use Whaai\WhaaiApi\Api\Wrappers\PaymentGateways\PaymentTransactions;
use Whaai\WhaaiApi\Api\Wrappers\Sheets\SheetItems;
use Whaai\WhaaiApi\Api\Wrappers\Sheets\Sheets;
use Whaai\WhaaiApi\Api\Wrappers\ShippingGateways\PackageTypes;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketCalculators;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketCategories;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketEvents;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\Tickets;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketTransactions;

abstract class ApiEndpointsForCache
{
    /**
     * @var ContactCategories
     */
    protected $contactCategories;

    /**
     * @var Contacts
     */
    protected $contacts;

    /**
     * @var Files
     */
    protected $files;

    /**
     * @var CmsWebsites
     */
    protected $cmsWebsites;

    /**
     * @var InventoryItems
     */
    protected $inventoryItems;

    /**
     * @var InventoryPrices
     */
    protected $inventoryPrices;

    /**
     * @var InventoryCategories
     */
    protected $inventoryCategories;

    /**
     * @var Countries
     */
    protected $countries;

    /**
     * @var Addresses
     */
    protected $addresses;

    /**
     * @var Tickets
     */
    protected $tickets;

    /**
     * @var TicketEvents
     */
    protected $ticketEvents;

    /**
     * @var TicketCategories
     */
    protected $ticketCategories;

    /**
     * @var TicketCalculators
     */
    protected $ticketCalculators;

    /**
     * @var TicketTransactions
     */
    protected $ticketTransactions;

    /**
     * @var PaymentMethods
     */
    protected $paymentMethods;

    /**
     * @var InventoryOptions
     */
    protected $inventoryOptions;

    /**
     * @var InventoryPurchaseOrders
     */
    protected $inventoryPurchaseOrders;

    /**
     * @var PaymentTransactions
     */
    protected $paymentTransactions;

    /**
     * @var Sheets
     */
    protected $sheets;

    /**
     * @var SheetItems
     */
    protected $sheetItems;

    /**
     * @var Identifications
     */
    protected $identifications;

    /**
     * @var PackageTypes
     */
    protected $packageTypes;

    public function __construct()
    {
        $this->contactCategories = app(ContactCategories::class);
        $this->contacts = app(Contacts::class);
        $this->files = app(Files::class);
        $this->cmsWebsites = app(CmsWebsites::class);
        $this->inventoryItems = app(InventoryItems::class);
        $this->inventoryPrices = app(InventoryPrices::class);
        $this->inventoryCategories = app(InventoryCategories::class);
        $this->countries = app(Countries::class);
        $this->addresses = app(Addresses::class);
        $this->tickets = app(Tickets::class);
        $this->ticketEvents = app(TicketEvents::class);
        $this->ticketCategories = app(TicketCategories::class);
        $this->ticketCalculators = app(TicketCalculators::class);
        $this->ticketTransactions = app(TicketTransactions::class);
        $this->paymentMethods = app(PaymentMethods::class);
        $this->inventoryOptions = app(InventoryOptions::class);
        $this->inventoryPurchaseOrders = app(InventoryPurchaseOrders::class);
        $this->paymentTransactions = app(PaymentTransactions::class);
        $this->sheets = app(Sheets::class);
        $this->sheetItems = app(SheetItems::class);
        $this->identifications = app(Identifications::class);
        $this->packageTypes = app(PackageTypes::class);
    }

    public abstract function updateCache();
}