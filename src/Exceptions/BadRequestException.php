<?php

namespace Whaai\WhaaiApi\Exceptions;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Message\ResponseInterface;
use Illuminate\Support\MessageBag;

class BadRequestException extends RequestException
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * @var ClientException
     */
    protected $e;

    public function __construct(ClientException $e)
    {
        $this->e = $e;
    }

    /**
     * @param ResponseInterface $response
     * @return self
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    public function setMessage()
    {
        $data = json_decode($this->response->getBody()->getContents(), true);

        if (json_last_error()){
            $this->message = $this->response->getBody()->getContents();
        } else {
            $this->message = array_get($data, 'message');
            $this->code = array_get($data, 'status_code');

            if (isset($data['errors'])){
                $this->errors = new MessageBag($data['errors']);
            }
        }

        $this->message .= $this->e->getMessage();

        return $this;
    }

    /**
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }
}