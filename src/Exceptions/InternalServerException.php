<?php

namespace Whaai\WhaaiApi\Exceptions;


use GuzzleHttp\Message\RequestInterface;
use GuzzleHttp\Message\ResponseInterface;

class InternalServerException extends RequestException
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var string
     */
    protected $error_details;

    /**
     * @param string $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param ResponseInterface $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    public function setMessage()
    {
        $this->error_details = json_encode($this->response->getBody()->getContents());

        if (json_last_error()){
            $this->message = "Error connecting, more details in the exception";
        } else {
            $this->message = "Error connecting to the API Server, please contact whaai administrators";
        }

        return $this;
    }

    public function getErrorDetails()
    {
        return $this->error_details;
    }
}