<?php

namespace Whaai\WhaaiApi;

use Artesaos\SEOTools\SEOTools;
use Whaai\WhaaiApi\Api\Data\Cms\CmsSocialProfile;
use Whaai\WhaaiApi\Api\Integrations\Debugbar\WhaaiCollector;
use Whaai\WhaaiApi\Api\Wrappers\Cms\CmsWebsites;
use Whaai\WhaaiApi\Api\Wrappers\Contacts\ContactCategories;
use Whaai\WhaaiApi\Api\Wrappers\Contacts\Contacts;
use Whaai\WhaaiApi\Api\Wrappers\Countries;
use Whaai\WhaaiApi\Api\Wrappers\CouponApplications;
use Whaai\WhaaiApi\Api\Wrappers\Coupons;
use Whaai\WhaaiApi\Api\Wrappers\Fields\Addresses;
use Whaai\WhaaiApi\Api\Wrappers\Files\Files;
use Whaai\WhaaiApi\Api\Wrappers\Identifications;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryCategories;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryItems;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryOptions;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryPrices;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryPurchaseOrders;
use Whaai\WhaaiApi\Api\Wrappers\Inventory\InventoryStocks;
use Whaai\WhaaiApi\Api\Wrappers\PaymentGateways\PaymentMethods;
use Whaai\WhaaiApi\Api\Wrappers\PaymentGateways\PaymentTransactions;
use Whaai\WhaaiApi\Api\Wrappers\Sheets\SheetItems;
use Whaai\WhaaiApi\Api\Wrappers\Sheets\Sheets;
use Whaai\WhaaiApi\Api\Wrappers\ShippingGateways\PackageTypes;
use Whaai\WhaaiApi\Api\Wrappers\ShippingGateways\ShippingGateways;
use Whaai\WhaaiApi\Api\Wrappers\Standalone;
use Whaai\WhaaiApi\Api\Wrappers\Taxes;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketActivities;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketCalculators;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketCategories;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketEvents;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\Tickets;
use Whaai\WhaaiApi\Api\Wrappers\Tickets\TicketTransactions;
use Whaai\WhaaiApi\Console\BackupDatabaseCommand;
use Whaai\WhaaiApi\Console\CleanStorageFolder;
use Whaai\WhaaiApi\Console\ResetDatabase;
use Whaai\WhaaiApi\Console\UpdateApiCache;
use Whaai\WhaaiApi\Console\UpdateFilesCache;
use Config;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class WhaaiApiServiceProvider extends ServiceProvider
{
    protected $defer = false;

    protected $commands = [
        UpdateFilesCache::class,
        BackupDatabaseCommand::class,
        CleanStorageFolder::class,
        UpdateApiCache::class,
        ResetDatabase::class,
    ];

    protected $api_wrappers = [
        ContactCategories::class,
        Contacts::class,
        Files::class,
        CmsWebsites::class,
        InventoryItems::class,
        InventoryPrices::class,
        InventoryCategories::class,
        Countries::class,
        Addresses::class,
        TicketEvents::class,
        TicketActivities::class,
        TicketCategories::class,
        TicketCalculators::class,
        TicketTransactions::class,
        PaymentMethods::class,
        InventoryOptions::class,
        InventoryPurchaseOrders::class,
        PaymentTransactions::class,
        Sheets::class,
        SheetItems::class,
        Identifications::class,
        PackageTypes::class,
        Tickets::class,
        Coupons::class,
        CouponApplications::class,
        InventoryStocks::class,
        ShippingGateways::class,
        Taxes::class,
        Standalone::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/whaai-api.php';
        $this->mergeConfigFrom($configPath, 'whaai-api');
        $this->publishes([$configPath => config_path('whaai-api.php')], 'config');

        foreach ($this->api_wrappers as $wrapper) {
            $this->app->singleton($wrapper, function ($app) use ($wrapper) {
                $config = $app['config']['whaai-api'];

                return new $wrapper($config['tenant_token'], $config['user_token'], $config['api_url'], $config['cache_duration']);
            });
        }

        $this->commands($this->commands);
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $migrationPath = __DIR__.'/../../database/migrations';
        $this->publishes([
            $migrationPath => base_path('database/migrations'),
        ], 'migrations');

        if ($this->app->bound('debugbar')){
            $debugBar = $this->app->make('debugbar');
            $debugBar->addCollector(new WhaaiCollector());
        }

        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('whaai_api:update_files_cache')->everyFiveMinutes()->withoutOverlapping();
            $schedule->command('whaai_api:backup_database')->cron('0 4,11,17,23 * * *');
            $schedule->command('whaai_api:clean_storage_folder')->daily();
            $schedule->command('whaai_api:update_api_cache')->everyFiveMinutes()->withoutOverlapping();
        });
        $this->setSEOFields();
    }

    public function setSEOFields()
    {
        if (app()->runningInConsole() == false && $this->app->bound('seotools') && config('whaai-api.website_id')) {
            $websites = $this->app[CmsWebsites::class];
            $website = $websites->find(config('whaai-api.website_id'));

            Config::set('seotools.meta.defaults.title', $website->seo_website_name);
            Config::set('seotools.meta.defaults.keywords', explode(',', $website->seo_keywords));

            Config::set('seotools.opengraph.defaults.title', $website->seo_website_name);
            Config::set('seotools.opengraph.defaults.description', $website->seo_description);

            /** @var SEOTools $seoTools */
            $seoTools = $this->app['seotools'];
            $seoTools->setTitle($website->seo_default_title);
            $seoTools->setDescription($website->seo_description);

            $twitter = $website->social_profiles->first(function (CmsSocialProfile $socialProfile) {
                return $socialProfile->type == "twitter";
            });

            if ($twitter) {
                $seoTools->twitter()->setSite($twitter->username);
                $seoTools->twitter()->setUrl($twitter->url);
            }
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'command.whaai-api.update-files-cache',
            'command.whaai-api.backup-database',
            'command.whaai-api.clean-storage-folder',
        ];
    }
}