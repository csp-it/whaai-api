<?php

namespace Whaai\WhaaiApi\Api\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class ApiRequestCache extends Model
{
    protected $table = "api_requests_cache";
    protected $fillable = ['uri'];
    protected $casts = [
        'api_response' => 'array',
    ];

    public static function findByUri($uri)
    {
        $key = "api_key_" . str_slug($uri);
        return Cache::remember($key, config('whaai-api.cache_duration'), function () use ($uri) {
            return self::firstOrNew([
                'uri' => $uri,
            ]);
        });
    }

    public static function removeCachedItems($prefix)
    {
        $items = self::where('uri', 'like', $prefix . '%')->get();
        $items->each(function (ApiRequestCache $cache) {
            $cache->delete();
            Cache::forget($cache->getRememberKey());
        });
    }

    public function getRememberKey()
    {
        return 'api_key_' . str_slug($this->uri);
    }

    public function saveAndRemember()
    {
        try {
            $this->updated_at = new Carbon();
            $this->save();

            Cache::put($this->getRememberKey(), $this, Carbon::now()->addMinutes(config('whaai-api.cache_duration')));
        } catch (QueryException $exception) {
            Artisan::call('cache:clear');
        }
    }
}