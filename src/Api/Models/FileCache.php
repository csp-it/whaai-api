<?php

namespace Whaai\WhaaiApi\Api\Models;

use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FileCache extends Model
{
    protected $table = "api_files_cache";
    protected $dates = [
        'last_update_on_server',
    ];

    public static function linkByUuid($uuid, $size = '100')
    {
        $file = static::findByUuid($uuid);
        if ($file) {
            if (config('whaai-api.disable_files_cache', false) && $file->public_link) {
                if ($size == "original") {
                    return $file->public_link;
                } else {
                    return $file->public_link . '?size=' . $size;
                }
            } else {
                $prefix = config('whaai-api.prefix_url_for_synced_files');
                if ($size == "original") {
                    return url($prefix . '/' . $file->local_file_path . '/' . $file->filename);
                } else {
                    return url($prefix . '/' . $file->local_file_path . '/' . $size . '-' . $file->filename);
                }
            }
        }

        return '#';
    }

    public static function findByUuid($uuid)
    {
        if ($uuid){
            $cache = Cache::get('files_cache', null);
            if (is_null($cache)){
                $all = self::select(['local_file_path', 'filename', 'file_uuid', 'public_link'])->get();
                $cache = [];
                foreach ($all as $file) {
                    $cache[$file->file_uuid] = $file;
                }
                Cache::put('files_cache', $cache, Carbon::now()->addMinutes(config('whaai-api.cache_duration')));
            }

            return array_get($cache, $uuid);
        }
    }

    public static function findByUuidInDatabase($uuid)
    {
        self::unguard();
        return self::firstOrNew([
            'file_uuid' => $uuid,
        ]);
    }
}