<?php

namespace Whaai\WhaaiApi\Helpers\Coupons;

use Whaai\WhaaiApi\Api\Data\Coupons\Coupon;

class CouponApplicationResult
{
    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * @var boolean
     */
    protected $valid;

    /**
     * @var array
     */
    protected $errors;

    public function __construct($result)
    {
        $this->valid = isset($result['valid']) && $result['valid'];
        $this->coupon = new Coupon(array_get($result, 'coupon', []));
        $this->errors = array_get($result, 'errors');
    }

    /**
     * @return Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}