<?php

namespace Whaai\WhaaiApi\Helpers\Tickets;


class PurchasedTicket implements \JsonSerializable
{
    protected $ticket_type_id;
    protected $quantity;

    /**
     * PurchasedTicket constructor.
     * @param $ticket_type_id
     * @param $quantity
     */
    public function __construct($ticket_type_id, $quantity)
    {
        $this->ticket_type_id = $ticket_type_id;
        $this->quantity = $quantity;
    }


    /**
     * @return mixed
     */
    public function getTicketTypeId()
    {
        return $this->ticket_type_id;
    }

    /**
     * @param mixed $ticket_type_id
     * @return PurchasedTicket
     */
    public function setTicketTypeId($ticket_type_id)
    {
        $this->ticket_type_id = $ticket_type_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return PurchasedTicket
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'db_id' => $this->ticket_type_id,
            'quantity' => $this->quantity,
        ];
    }
}