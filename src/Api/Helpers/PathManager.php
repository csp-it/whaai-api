<?php

namespace Whaai\WhaaiApi\Helpers;

use Illuminate\Contracts\Filesystem\Filesystem;
use Ramsey\Uuid\Uuid;

class PathManager
{
    protected $filename;
    protected $folder;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    public function __construct($filename = null)
    {
        $uuid = Uuid::uuid4();
        $this->folder = substr($uuid, 0, 2) . '/' . substr($uuid, 2, 2) . '/' . substr($uuid, 4);
        $this->filename = $filename;
        $this->filesystem = app('files');
    }

    //********************************************************************************************//

    public function getFullPath()
    {
        return $this->getPublicFullPathOnServer() . '/' . $this->filename;
    }

    public function getPublicFullPathOnServer()
    {
        $path = public_path($this->getPublicPath());
        if (!$this->filesystem->isDirectory($path)) {
            $this->filesystem->makeDirectory($path, 0755, true);
        }
        return $path;
    }

    public function getPublicPath()
    {
        return 'public-uploads/' . $this->folder;
    }

    public function getUrl()
    {
        return url('public-uploads/' . $this->folder . '/' . $this->filename);
    }

    //********************************************************************************************//

    public function getTempUrl()
    {
        return url($this->getPublicTempPath() . '/' . $this->filename);
    }

    public function getPublicTempPath()
    {
        return 'public-temp/' . $this->folder;
    }

    public function writeInTempPublic($content)
    {
        file_put_contents($this->getPublicTempFullPathOnServer(), $content);
    }

    public function getPublicTempFullPathOnServer()
    {
        $path = public_path($this->getPublicTempPath());
        if (!$this->filesystem->isDirectory($path)) {
            $this->filesystem->makeDirectory($path, 0755, true);
        }
        return $path;
    }

    //********************************************************************************************//

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     *
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function writeInPrivate($content)
    {
        file_put_contents($this->getPrivatePath(), $content);
    }

    public function getPrivatePath()
    {
        $path = storage_path('app/global/' . $this->folder);
        if (!$this->filesystem->isDirectory($path)) {
            $this->filesystem->makeDirectory($path, 0755, true);
        }

        if ($this->filename) {
            return $path . '/' . $this->filename;
        }

        return $path;
    }
}