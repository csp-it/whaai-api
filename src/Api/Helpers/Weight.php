<?php

namespace Whaai\WhaaiApi\Helpers;

use PhpUnitsOfMeasure\PhysicalQuantity\Mass;

class Weight
{
    public static $round_to_x_decimals = 2;
    public static $decimals_to_display = 2;

    public static $unit_icons = [
        'kg' => 'kg',
        'oz' => 'oz',
        'lbs' => 'lbs',
    ];

    public $weight;
    public $unit;
    public $weight_kg;
    public $weight_lbs;
    public $weight_oz;

    public function __construct($weight = 0, $unit = "kg")
    {
        $unit = strtolower($unit);
        $this->weight = $weight;
        $this->unit = $unit;
        $quantity = new Mass($weight, $unit);
        $this->weight_kg = $quantity->toUnit('kg');
        $this->weight_lbs = $quantity->toUnit('lbs');
        $this->weight_oz = $quantity->toUnit('oz');
    }

    public static function getList($first_empty = true)
    {
        if ($first_empty) {
            return ['' => ''] + static::$unit_icons;
        }

        return static::$unit_icons;
    }

    public function getFormattedWeightForView($round = false)
    {
        return $this->getUnitIcon() . $this->getWeightForView($round);
    }

    public function getUnitIcon()
    {
        $key = $this->unit;
        $key = strtolower($key);
        if (isset(static::$unit_icons[$key])) {
            return static::$unit_icons[$key];
        }
        return '';
    }

    public function getWeightForView($round = false)
    {
        $weight = $this->weight;
        if ($round) {
            return number_format(round($weight, static::$round_to_x_decimals), 0);
        }

        return number_format(round($weight, static::$round_to_x_decimals), static::$decimals_to_display);
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function getWeightInKg()
    {
        return $this->weight_kg;
    }

    public function getWeightInOz()
    {
        return $this->weight_oz;
    }

    public function getWeightInLbs()
    {
        return $this->weight_lbs;
    }

    public function getWeightForInput()
    {
        return number_format($this->weight, 2, '.', '');
    }

    public function add($weight = 0, $unit = 'kg')
    {
        $unit = strtolower($unit);
        if ($unit == $this->unit) {
            $this->weight = $this->weight + $weight;
        } else {
            $quantity = new Mass($weight, $unit);
            $newWeight = $quantity->toUnit($this->unit);
            $this->weight = $this->weight + $newWeight;
        }
        $quantity = new Mass($this->weight, $this->unit);
        $this->weight_kg = $quantity->toUnit('kg');
        $this->weight_lbs = $quantity->toUnit('lbs');
        $this->weight_oz = $quantity->toUnit('oz');
    }


}