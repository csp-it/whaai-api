<?php

namespace Whaai\WhaaiApi\Helpers\Views\Menu;

use Whaai\WhaaiApi\Api\Data\Cms\CmsMenuItem;

class MenuRenderer
{
    public $website;

    public function __construct($website)
    {
        $this->website = $website;
    }

    public function renderTopMenu()
    {
        return $this->renderMenu(1);
    }

    private function renderMenu($id)
    {
        $menu = $this->website->getMenu($id);

        $renderedMenu = collect();
        $menu->items->filter(function (CmsMenuItem $item) {
            return $item->parent_id == 0 && $item->content_type;
        })->each(function (CmsMenuItem $item) use ($renderedMenu) {
            $renderedMenu->push(new MenuItem(view('app.menus.' . $item->content_type, compact('item'))->render()));
        });

        return $renderedMenu;
    }

    public function renderMainMenu()
    {
        return $this->renderMenu(2);
    }

    public function renderFooterMenu1()
    {
        return $this->renderMenu(3);
    }

    public function renderFooterMenu2()
    {
        return $this->renderMenu(4);
    }
}