<?php

namespace Whaai\WhaaiApi\Helpers\Views\Menu;


class MenuItem
{
    /**
     * @var string
     */
    public $html;

    public function __construct($html)
    {
        $this->html = $html;
    }
}