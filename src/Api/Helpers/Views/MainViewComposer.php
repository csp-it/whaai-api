<?php

namespace Whaai\WhaaiApi\Helpers\Views;

use Illuminate\Contracts\View\View;

abstract class MainViewComposer
{
    public $loaded_data = null;

    public function compose(View $view)
    {
        if (is_null($this->loaded_data)) {
            $this->loadProperties();
            $this->loaded_data = $this->composedData();
        }

        $view->with($this->loaded_data);
    }

    /**
     * Add here the properties that must be filled int that came from the whaai api
     */
    public abstract function loadProperties();

    /**
     * Returns an array with the required data for the view
     * @return array
     */
    public abstract function composedData();
}