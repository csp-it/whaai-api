<?php

namespace Whaai\WhaaiApi\Api\Collections;

use Api\Transformers\Fields\BaseTransformer;
use JsonSerializable;
use Illuminate\Support\Collection;

abstract class BaseCollection extends Collection implements JsonSerializable
{
    public function jsonSerialize()
    {
        return $this->convert();
    }

    public function convert()
    {
        $items = [];
        /** @var BaseTransformer $item */
        foreach ($this->items as $item) {
            $items[] = $item->serializeForApi();
        }

        return [
            'items' => $items,
        ];
    }
}