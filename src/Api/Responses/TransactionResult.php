<?php

namespace Whaai\WhaaiApi\Api\Responses;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\PaymentGateways\PaymentTransaction;

class TransactionResult
{
    /**
     * @var boolean
     */
    protected $successful;

    /**
     * @var boolean
     */
    protected $cancelled;

    /**
     * @var boolean
     */
    protected $redirect;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var PaymentTransaction
     */
    protected $transaction;

    public function __construct(Connector $connector, $response)
    {
        $this->successful = (bool) $response['status']['is_successful'];
        $this->cancelled = (bool) $response['status']['is_cancelled'];
        $this->redirect = (bool) $response['status']['is_redirect'];
        $this->message = $response['response']['message'];
        $this->data = $response['response']['data'];
        $this->transaction = new PaymentTransaction($connector, $response['transaction']);
    }

    /**
     * @return boolean
     */
    public function isSuccessful()
    {
        return $this->successful;
    }

    /**
     * @return boolean
     */
    public function isCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @return boolean
     */
    public function isRedirect()
    {
        return $this->redirect;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return PaymentTransaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}