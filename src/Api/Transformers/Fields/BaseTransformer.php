<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 8/11/2016
 * Time: 12:19 AM
 */

namespace Whaai\WhaaiApi\Api\Transformers\Fields;


class BaseTransformer
{
    public function serializeForApi()
    {
        return get_object_vars($this);
    }
}