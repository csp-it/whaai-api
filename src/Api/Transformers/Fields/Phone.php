<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 8/11/2016
 * Time: 12:07 AM
 */

namespace Whaai\WhaaiApi\Api\Transformers\Fields;

class Phone extends BaseTransformer
{
    protected $type;
    protected $cc;
    protected $number;
    protected $ext;

    /**
     * Phone constructor.
     * @param $type
     * @param $phone_cc
     * @param $phone_number
     * @param null $ext
     */
    public function __construct($type, $phone_cc, $phone_number, $ext = null)
    {
        $this->type = $type;
        $this->cc = $phone_cc;
        $this->number = $phone_number;
        $this->ext = $ext;
    }
}