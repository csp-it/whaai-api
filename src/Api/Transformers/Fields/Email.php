<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 8/11/2016
 * Time: 12:07 AM
 */

namespace Whaai\WhaaiApi\Api\Transformers\Fields;

class Email extends BaseTransformer
{
    protected $type;
    protected $email;

    /**
     * Email constructor.
     * @param $type
     * @param $email
     */
    public function __construct($type, $email)
    {
        $this->type = $type;
        $this->email = $email;
    }
}