<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 8/11/2016
 * Time: 12:07 AM
 */

namespace Whaai\WhaaiApi\Api\Transformers\Fields;

class Address extends BaseTransformer
{
    protected $street;
    protected $street2;
    protected $housenumber;
    protected $suburb;
    protected $city;
    protected $zip;
    protected $state;
    protected $country;
    protected $type;

    /**
     * Address constructor.
     * @param $street
     * @param $street2
     * @param $housenumber
     * @param $suburb
     * @param $city
     * @param $zip
     * @param $state
     * @param $country
     * @param $type
     */
    public function __construct($street, $street2, $housenumber, $suburb, $city, $zip, $state, $country, $type)
    {
        $this->street = $street;
        $this->street2 = $street2;
        $this->housenumber = $housenumber;
        $this->suburb = $suburb;
        $this->city = $city;
        $this->zip = $zip;
        $this->state = $state;
        $this->country = $country;
        $this->type = $type;
    }


}