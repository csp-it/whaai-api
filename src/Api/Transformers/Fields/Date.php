<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 8/11/2016
 * Time: 12:07 AM
 */

namespace Whaai\WhaaiApi\Api\Transformers\Fields;

use Carbon\Carbon;

class Date extends BaseTransformer implements \JsonSerializable
{
    public $day;
    public $month;
    public $year;

    /**
     * Date constructor.
     * @param Carbon $date
     */
    public function __construct(Carbon $date)
    {
        $this->day = $date->day;
        $this->month = $date->month;
        $this->year = $date->year;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return $this->serializeForApi();
    }
}