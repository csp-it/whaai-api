<?php

namespace Whaai\WhaaiApi\Api\Data;

use ArrayAccess;
use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Models\ApiRequestCache;
use Whaai\WhaaiApi\Exceptions\BadRequestException;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;

class BaseModel implements ArrayAccess
{
    /**
     * @var array
     */
    protected $belongs_to = [];

    /**
     * @var array
     */
    protected $has_many = [];

    /**
     * @var Connector
     */
    protected $connector;
    protected $attributes;
    protected $index_name;

    /**
     * @var MessageBag
     */
    protected $errors;

    public function __construct($connector, $attributes = [])
    {
        $this->connector = $connector;
        $this->attributes = $attributes;
        $this->errors = new MessageBag();
    }

    /**
     * @return boolean
     */
    public function save()
    {
        try {
            if ($this->id){
                $response = $this->connector->put($this->connector->getPrefix() . '/' . $this->id, $this->attributes);
            } else {
                $response = $this->connector->post($this->connector->getPrefix(), $this->attributes);
            }
            $this->fill($response[$this->index_name]);
            $this->cleanCacheForSavedItem();
            return true;
        } catch (BadRequestException $exception) {
            $this->errors = $exception->getErrors();
        }

        return false;
    }

    public function fill($fields)
    {
        foreach ($fields as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function cleanCacheForSavedItem()
    {
        ApiRequestCache::removeCachedItems($this->connector->getPrefix());
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function __get($var)
    {
        if (isset($this->attributes[$var])) {
            if (isset($this->belongs_to[$var])){
                return $this->asBelongsTo($var);
            } else if(isset($this->has_many[$var])) {
                return $this->asHasMany($var);
            } else {
                return $this->attributes[$var];
            }
        }

        return null;
    }

    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    protected function asBelongsTo($var)
    {
        $class = $this->belongs_to[$var];
        return new $class($this->connector, $this->attributes[$var]);
    }

    protected function asHasMany($var)
    {
        $class = $this->has_many[$var];
        $collection = new Collection();
        foreach ($this->attributes[$var] as $item) {
            $collection->push(new $class($this->connector, $item));
        }

        return $collection;
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    /**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->attributes[$key]);
    }

    public function __toString()
    {
        return json_encode($this->attributes);
    }
}