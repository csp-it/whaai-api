<?php

namespace Whaai\WhaaiApi\Api\Data\Inventory;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Files\File as FileAPI;

class InventoryPurchaseOrderItem extends BaseModel
{
    
    public $belongs_to = [
        'item' => InventoryItem::class,
    ];
}