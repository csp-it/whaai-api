<?php

namespace Whaai\WhaaiApi\Api\Data\Inventory;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Files\File as FileAPI;

class InventoryItem extends BaseModel
{
    public $index_name = "inventory_items";

    public $has_many = [
        'stocks' => InventoryStock::class,
        'options' => InventoryOption::class,
        'prices' => InventoryPrice::class,
        'images_api' => FileAPI::class,
        'files_api' => FileAPI::class,
        'combinations' => InventoryCombination::class,
    ];

    public $belongs_to = [
        'category' => InventoryCategory::class,
        'barcode' => InventoryBarCode::class,
    ];
}