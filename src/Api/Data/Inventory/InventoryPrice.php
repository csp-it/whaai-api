<?php

namespace Whaai\WhaaiApi\Api\Data\Inventory;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class InventoryPrice extends BaseModel
{
    public $item;
    public $index_name = "price";
}