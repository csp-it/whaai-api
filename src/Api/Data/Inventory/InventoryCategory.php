<?php

namespace Whaai\WhaaiApi\Api\Data\Inventory;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class InventoryCategory extends BaseModel
{
    public $index_name = "inventory_categories";


    public $has_many = [
        'childs' => InventoryCategory::class,

    ];

    public $belongs_to = [
        'parent' => InventoryCategory::class,
    ];
}