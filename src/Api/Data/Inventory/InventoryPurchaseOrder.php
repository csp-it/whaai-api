<?php

namespace Whaai\WhaaiApi\Api\Data\Inventory;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Contacts\Contact;
use Whaai\WhaaiApi\Api\Data\Coupons\CouponApplication;
use Whaai\WhaaiApi\Api\Data\Fields\Address;
use Whaai\WhaaiApi\Api\Data\PaymentGateways\PaymentTransaction;
use Whaai\WhaaiApi\Api\Data\ShippingGateways\ShippingTransaction;
use Whaai\WhaaiApi\Api\Data\Tax;

class InventoryPurchaseOrder extends BaseModel
{
    public $index_name = "inventory_po";

    public $has_many = [
        'items' => InventoryPurchaseOrderItem::class,
        'coupon_applications' => CouponApplication::class,
    ];

    public $belongs_to = [
        'contact' => Contact::class,
        'tax' => Tax::class,
        'shipping_address' => Address::class,
        'billing_address' => Address::class,
        'payment_transaction' => PaymentTransaction::class,
        'shipping_transaction' => ShippingTransaction::class,
    ];

    public function cancel()
    {
        $this->connector->get($this->connector->getPrefix() . '/' . $this->id . '/cancel');
        
        return true;
    }

    public function getPdfUrl()
    {
        $response = $this->connector->get($this->connector->getPrefix().'/'.$this->id.'/pdf');
        return $response;
    }
}