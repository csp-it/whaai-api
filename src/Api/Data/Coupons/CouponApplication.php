<?php

namespace Whaai\WhaaiApi\Api\Data\Coupons;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class CouponApplication extends BaseModel
{
    public $index_name = "coupon_application";

    public $belongs_to = [
        'coupon' => Coupon::class,
    ];
}