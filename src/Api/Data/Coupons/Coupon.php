<?php

namespace Whaai\WhaaiApi\Api\Data\Coupons;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class Coupon extends BaseModel
{
    public $index_name = "coupon";

    public $has_many = [
        'applications' => CouponApplication::class,
    ];
}