<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 7/20/2016
 * Time: 11:34 PM
 */

namespace Whaai\WhaaiApi\Api\Data\Tickets;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Contacts\Contact;
use Whaai\WhaaiApi\Api\Data\Files\File as FileAPI;

class TicketEvent extends BaseModel
{
    public $index_name = "tickets_events";

    public $has_many = [
        'ticket_types' => TicketType::class,
        'images' => FileAPI::class,
        'ads' => FileAPI::class,
    ];

    public $belongs_to = [
        'category' => TicketCategory::class,
        'contact' => Contact::class,
    ];
}
