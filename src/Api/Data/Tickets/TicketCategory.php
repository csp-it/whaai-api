<?php

namespace Whaai\WhaaiApi\Api\Data\Tickets;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class TicketCategory extends BaseModel
{
    public $index_name = "tickets_categories";

}