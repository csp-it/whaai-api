<?php

namespace Whaai\WhaaiApi\Api\Data\Tickets;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Contacts\Contact;
use Whaai\WhaaiApi\Api\Data\PaymentGateways\PaymentTransaction;

class TicketCalculator extends BaseModel
{
    public $index_name = "tickets_calculator";
}
