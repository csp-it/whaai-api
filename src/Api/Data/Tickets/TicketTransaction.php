<?php
/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 7/20/2016
 * Time: 11:34 PM
 */

namespace Whaai\WhaaiApi\Api\Data\Tickets;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Contacts\Contact;
use Whaai\WhaaiApi\Api\Data\PaymentGateways\PaymentTransaction;

class TicketTransaction extends BaseModel
{
    public $index_name = "ticket_transaction";

    public $has_many = [
        'tickets' => Ticket::class,
    ];

    public $belongs_to = [
        'event' => Ticket::class,
        'contact' => Contact::class,
        'transaction' => PaymentTransaction::class,
    ];
}
