<?php

namespace Whaai\WhaaiApi\Api\Data\Tickets;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Contacts\Contact;
use Whaai\WhaaiApi\Api\Data\Files\File as FileAPI;

class TicketActivity extends BaseModel
{
    public $index_name = "tickets_activities";

    public $has_many = [
        'ticket_types' => TicketType::class,
        'images' => FileAPI::class,
        'ads' => FileAPI::class,
        'calendar_events' => TicketCalendarEvent::class
    ];

    public $belongs_to = [
        'category' => TicketCategory::class,
        'contact' => Contact::class,
    ];
}
