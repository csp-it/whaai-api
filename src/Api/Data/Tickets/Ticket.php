<?php

namespace Whaai\WhaaiApi\Api\Data\Tickets;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class Ticket extends BaseModel
{
    public $index_name = "ticket";

    public function getPdfUrl()
    {
        return $this->connector->get($this->connector->getPrefix() . '/' . $this->id . '/pdf');
    }
}