<?php

namespace Whaai\WhaaiApi\Api\Data\PaymentGateways;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class PaymentMethod extends BaseModel
{
    public $index_name = "payment_gateways_methods";

    public $belongs_to = [
        'gateway' => PaymentGateway::class,
    ];

    public function button($url, $button_label, $amount, $item_type, $item_id, $label = "", $description = "")
    {
        return $this->connector->post($this->connector->getPrefix() . '/' . $this->id . '/button', [
            'external_redirect' => url($url),
            'button_label' => $button_label,
            'amount' => $amount,
            'label' => $label,
            'description' => $description,
            'item_type' => $item_type,
            'item_id' => $item_id,
        ]);
    }

    public function form($url, $amount, $item_type, $item_id, $label = "", $description = "")
    {
        return $this->connector->post($this->connector->getPrefix() . '/' . $this->id . '/form', [
            'external_redirect' => url($url),
            'amount' => $amount,
            'label' => $label,
            'description' => $description,
            'item_type' => $item_type,
            'item_id' => $item_id,
        ]);
    }
}
