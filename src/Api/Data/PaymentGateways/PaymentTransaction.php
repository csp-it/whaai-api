<?php

namespace Whaai\WhaaiApi\Api\Data\PaymentGateways;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class PaymentTransaction extends BaseModel
{
    public $index_name = "payment_transaction";
}