<?php

namespace Whaai\WhaaiApi\Api\Data\ShippingGateways;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class ShippingTransaction extends BaseModel
{
    public $belongs_to = [
        'package_type' => PackageType::class,
        'gateway' => ShippingGateway::class,
    ];
}