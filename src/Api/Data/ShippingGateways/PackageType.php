<?php

namespace Whaai\WhaaiApi\Api\Data\ShippingGateways;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Files\File;

class PackageType extends BaseModel
{
    public $index_name = "shipping_gateways_package_types";

    public $belongs_to = [
        'gateway' => ShippingGateway::class,
    ];

    public $has_many = [
        'images' => File::class
    ];

    public function getRate($address_id, $weight, $weightUnit = 'kg')
    {
        $response = $this->connector->post($this->connector->getPrefix() . '/' . $this->id . '/rates', [
            'address_id' => $address_id,
            'weight' => $weight,
            'weight_unit' => $weightUnit,
        ]);

        return $response;
    }
}
