<?php

namespace Whaai\WhaaiApi\Api\Data\ShippingGateways;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Exceptions\BadRequestException;

class ShippingGateway extends BaseModel
{
    public function validateAddress($name, $street, $street2, $city, $zip, $country)
    {
        $endpoint = $this->connector->getPrefix() . "/" . $this->id . "/addresses/validate";
        try {
            return $this->connector->post($endpoint, compact(
                'name', 'street', 'street2', 'city', 'zip', 'country'
            ));
        } catch (BadRequestException $exception) {
            return [
                'city_state_zip_code_valid' => false,
                'extra_message' => implode(', ', $exception->getErrors()->all()),
            ];
        }
    }
}