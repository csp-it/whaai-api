<?php

namespace Whaai\WhaaiApi\Api\Data\Files;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Models\FileCache;
use Carbon\Carbon;
use File as LaravelFile;

class File extends BaseModel
{
    public static $thumbs = [
        '50',
        '100',
        '500',
        '1000',
    ];

    public function saveInCache()
    {
        $cacheRecord = FileCache::findByUuidInDatabase($this->uuid);
        $create = true;
        if ($cacheRecord->exists && $cacheRecord->last_update_on_server) {
            $lastUpdateOnServer = new Carbon($this->updated_at);
            $lastUpdateOnCache = $cacheRecord->last_update_on_server;
            $create = $lastUpdateOnServer->ne($lastUpdateOnCache);
        }

        if ($create){
            $this->putFileInLocalCache($cacheRecord);
        }
    }

    protected function putFileInLocalCache($cacheRecord)
    {
        $path = $this->getFinalPath();
        $filenamePretty = str_slug($this->label) . '.' . $this->extension;
        foreach (static::$thumbs as $thumb) {
            if ($this->{"thumb_" . $thumb}) {
                try {
                    $content = file_get_contents($this->public_link . '?size=' . $thumb);
                    file_put_contents($path . '/' . $thumb . '-' . $filenamePretty, $content);
                } catch (\ErrorException $e) {
                    echo $e->getMessage();
                }
            }
        }

        try {
            $content = file_get_contents($this->public_link);
            file_put_contents($path . '/' . $filenamePretty, $content);
            $cacheRecord->file_uuid = $this->uuid;
            $cacheRecord->local_file_path = $this->getRelativePath();
            $cacheRecord->extension = $this->extension;
            $cacheRecord->last_update_on_server = $this->updated_at;
            $cacheRecord->public_link = $this->public_link;
            $cacheRecord->filename = $filenamePretty;
            $cacheRecord->save();
        } catch (\ErrorException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return string
     */
    protected function getFinalPath()
    {
        $path = config('whaai-api.path_for_synced_files');
        $finalPath = $path . '/' . $this->getRelativePath();
        LaravelFile::deleteDirectory($finalPath);
        @mkdir($finalPath, 0755, true);

        return $finalPath;
    }

    protected function getRelativePath()
    {
        $date = new Carbon($this->updated_at);
        return $date->year . '/' . $date->month . '/' . $date->day . '/' . $this->id;
    }

    public function getLink($thumb_size = '100')
    {
        return FileCache::linkByUuid($this->uuid, $thumb_size);
    }
}