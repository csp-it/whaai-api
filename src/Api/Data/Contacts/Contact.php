<?php

namespace Whaai\WhaaiApi\Api\Data\Contacts;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class Contact extends BaseModel
{
    public $contact_category;
    public $index_name = "contact";
}