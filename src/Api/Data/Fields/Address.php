<?php

namespace Whaai\WhaaiApi\Api\Data\Fields;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Wrappers\Identifications;

class Address extends BaseModel
{
    public $index_name = "address";

    public function save()
    {
        // Validate the zip code against state, only for US country
        if ($this->country == "us") {
            $this->state = strtoupper($this->state);
            $this->validateZipCodeWithState();
        }

        if ($this->errors->isEmpty()) {
            return parent::save();
        }

        return false;
    }

    public function validateZipCodeWithState()
    {
        /** @var Identifications $identifications */
        $identifications = app(Identifications::class);
        $entity = $identifications->entity('us_zip', 'zip', $this->zip);
        if ($entity['exists'] == false) {
            $this->errors->add('zip', 'The zip code is not a valid US zip code');
        } else if ($entity['state'] != $this->state) {
            $this->errors->add('zip', "The zip code doesn't match with the state entered");
        }
    }
}