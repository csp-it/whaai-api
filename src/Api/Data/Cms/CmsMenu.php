<?php

namespace Whaai\WhaaiApi\Api\Data\Cms;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class CmsMenu extends BaseModel
{
    public $belongs_to = [

    ];

    public $has_many = [
        'items' => CmsMenuItem::class,
    ];
}