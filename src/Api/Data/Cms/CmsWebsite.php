<?php

namespace Whaai\WhaaiApi\Api\Data\Cms;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Data\Files\File as FileAPI;

class CmsWebsite extends BaseModel
{
    public $index_name = "cms_websites";

    public $has_many = [
        'menus' => CmsMenu::class,
        'files' => FileAPI::class,
        'languages' => CmsLanguage::class,
        'social_profiles' => CmsSocialProfile::class,
    ];


    public $belongs_to = [
        'ecommerce_website' => EcommerceWebsite::class,
    ];

    public function getMenu($id)
    {
        return $this->menus->first(function ($item) use ($id) {
            return $item->id == $id;
        });
    }

    public function getPageContentForHandle($handle,$language)
    {
        foreach ($this->menus as $menu) {
            foreach ($menu->items as $item) {
                if($item->handle == $handle)
                {
                    return $item->getHtmlContent($language);
                }
            }
        }
        return null;
    }

    public function getPageTitleForHandle($handle,$language)
    {
        foreach ($this->menus as $menu) {
            foreach ($menu->items as $item) {
                if($item->handle == $handle)
                {
                    return $item->getTranslationLabel($language);
                }
            }
        }
        return null;
    }
}