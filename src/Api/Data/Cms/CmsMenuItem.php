<?php

namespace Whaai\WhaaiApi\Api\Data\Cms;

use Whaai\WhaaiApi\Api\Data\BaseModel;

class CmsMenuItem extends BaseModel
{
    public $has_many = [
        'childs' => CmsMenuItem::class,
        'translations' => CmsMenuItemTranslation::class,
        'text_content' => CmsTextContent::class,
    ];

    public function getTranslationLabel($language)
    {
        $translation = $this->translations->filter(function($obj) use ($language) {
            return $obj->language == $language;
        })->first();
        if($translation)
        {
            echo $translation->label;
        }

        return null;
    }

    public function getHtmlContent($language)
    {
        $translation = $this->text_content->filter(function($obj) use ($language) {
            return $obj->language == $language;
        })->first();
        if($translation)
        {
            echo $translation->content;
        }

        return null;
    }
}