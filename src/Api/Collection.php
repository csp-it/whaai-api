<?php


namespace Whaai\WhaaiApi\Api;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class Collection extends BaseCollection
{
    public function paginate($limit = 12)
    {
        $currentPage = Input::get('page', 1);
        return new LengthAwarePaginator(
            $this->forPage($currentPage, $limit),
            $this->count(),
            $limit,
            $currentPage, [
                'path' => Request::url(),
            ]
        );
    }

    public function find($id)
    {
        return $this->first(function ($item) use ($id) {
            return $item->id == $id;
        });
    }

    public function filterBy($key, $value)
    {
        return $this->filter(function ($item) use ($key, $value) {
            return $item->{$key} == $value;
        });
    }

    public function searchIn($key, $value)
    {
        return $this->filter(function ($item) use ($key, $value) {
            if (strpos(strtolower($item->{$key}), strtolower($value)) !== false) {
                return true;
            }
            return false;
        });
    }
}