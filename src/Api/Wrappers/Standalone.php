<?php

namespace Whaai\WhaaiApi\Api\Wrappers;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Country;
use Whaai\WhaaiApi\Api\Data\Single;

/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 7/13/2016
 * Time: 10:40 PM
 */
class Standalone extends Connector
{
    protected $index_paginated = false;

    public function get($url, $parameters = [])
    {
        $response = parent::get($url, $parameters);
        if (count($response) == 1) {
            return array_first($response);
        }

        return $response;
    }

    /**
     * Create new data instance
     * @param $item
     * @return Single
     */
    public function initObject($item = [])
    {
        return new Single($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return '';
    }
}