<?php

namespace Whaai\WhaaiApi\Api\Wrappers;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Coupons\CouponApplication;

class CouponApplications extends Connector
{
    protected $index_paginated = false;
    protected $use_cache = false;

    /**
     * Create new data instance
     * @param $item
     * @return CouponApplication
     */
    public function initObject($item = [])
    {
        return new CouponApplication($this, $item);
    }

    public function filter($item_type, $item_id)
    {
        return $this->where('item_type', $item_type)->where('item_id', $item_id);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'coupon-applications';
    }
}