<?php

namespace Whaai\WhaaiApi\Api\Wrappers;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Coupons\Coupon;
use Whaai\WhaaiApi\Helpers\Coupons\CouponApplicationResult;

class Coupons extends Connector
{
    protected $index_paginated = false;

    /**
     * @param $related_order
     * @param $item_id
     * @param $coupon_code
     * @return CouponApplicationResult
     */
    public function apply($related_order, $item_id, $coupon_code)
    {
        $result = $this->post($this->getPrefix() . '/apply-coupon', [
            'related_order' => $related_order,
            'item_id' => $item_id,
            'coupon_code' => $coupon_code,
        ]);

        return new CouponApplicationResult($result);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'coupons';
    }

    /**
     * Create new data instance
     * @param $item
     * @return Coupon
     */
    public function initObject($item = [])
    {
        return new Coupon($this, $item);
    }
}