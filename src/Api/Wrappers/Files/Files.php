<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Files;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Files\File as FileAPI;

class Files extends Connector
{
    public function getPrefix()
    {
        return 'files';
    }

    public function initObject($item = [])
    {
        return new FileAPI($this, $item);
    }
}