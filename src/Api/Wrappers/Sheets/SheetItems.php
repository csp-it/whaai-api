<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Sheets;

use Whaai\WhaaiApi\Api\Collection;
use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Sheets\Sheet;
use Whaai\WhaaiApi\Api\Data\Sheets\SheetItem;

class SheetItems extends Connector
{
    /**
     * @var Sheet
     */
    protected $sheet;

    public function getPrefix()
    {
        return 'sheets/' . $this->sheet->id . '/items';
    }

    public function initObject($item = [])
    {
        return new SheetItem($this, $item);
    }

    public function setSheet(Sheet $sheet)
    {
        $this->sheet = $sheet;
        $this->collection = new Collection();
        return $this;
    }

    public function getList()
    {
        return $this->all()->pluck('label', 'id')->all();
    }
}