<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Sheets;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Sheets\Sheet;

class Sheets extends Connector
{
    public function getPrefix()
    {
        return 'sheets';
    }

    public function initObject($item = [])
    {
        return new Sheet($this, $item);
    }
}