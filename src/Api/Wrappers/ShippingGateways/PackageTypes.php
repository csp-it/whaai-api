<?php

namespace Whaai\WhaaiApi\Api\Wrappers\ShippingGateways;

use Whaai\WhaaiApi\Api\Collection;
use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\ShippingGateways\PackageType;

class PackageTypes extends Connector
{
    public function getList()
    {
        return $this->all()->pluck('label', 'id')->all();
    }

    public function getListForWeight($weight = 0, $weightUnit = 'kg')
    {
        return $this->where('max_weight',$weight)->where('weight_unit',$weightUnit)->getCollection()->pluck('label', 'id')->all();
    }

    public function getPackageTypesForWeight($weight = 0, $weightUnit = 'kg')
    {
        return $this->where('max_weight', $weight)->where('weight_unit', $weightUnit)->all();
    }

    public function loadRates(Collection $types, $address_id, $weight, $weightUnit)
    {
        return $types->each(function (PackageType $packageType) use ($address_id, $weight, $weightUnit) {
            $rate = $packageType->getRate($address_id, $weight, $weightUnit);
            if (isset($rate['shipping_cost'])) {
                $packageType->shipping_rate = $rate['shipping_cost'];
            } else {
                $packageType->shipping_rate = false;
            }
        })->filter(function (PackageType $packageType) {
            return $packageType->shipping_rate;
        });
    }

    /**
     * Create new data instance
     * @param $item
     * @return PackageType
     */
    public function initObject($item = [])
    {
        return new PackageType($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'shipping-gateways/package-types';
    }
}