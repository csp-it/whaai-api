<?php

namespace Whaai\WhaaiApi\Api\Wrappers\ShippingGateways;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\ShippingGateways\ShippingGateway;

class ShippingGateways extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return ShippingGateway
     */
    public function initObject($item = [])
    {
        return new ShippingGateway($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'shipping-gateways/gateways';
    }
}