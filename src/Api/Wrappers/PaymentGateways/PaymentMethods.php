<?php

namespace Whaai\WhaaiApi\Api\Wrappers\PaymentGateways;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\PaymentGateways\PaymentMethod;

class PaymentMethods extends Connector
{
    protected $use_cache = false;

    public function getList()
    {
        return $this->all()->pluck('label', 'id')->all();
    }

    public function getListForCurrency($currency)
    {
        return $this->where('currency',$currency)->all()->pluck('label', 'id')->all();
    }

    /**
     * Create new data instance
     * @param $item
     * @return PaymentMethod
     */
    public function initObject($item = [])
    {
        return new PaymentMethod($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'payment-gateways/methods';
    }
}