<?php

namespace Whaai\WhaaiApi\Api\Wrappers\PaymentGateways;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\PaymentGateways\PaymentTransaction;

class PaymentTransactions extends Connector
{
    protected $use_cache = false;

    /**
     * Create new data instance
     * @param $item
     * @return PaymentTransaction
     */
    public function initObject($item = [])
    {
        return new PaymentTransaction($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'payment-gateways/payment-transactions';
    }
}