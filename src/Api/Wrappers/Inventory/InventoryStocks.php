<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Inventory;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryStock;

class InventoryStocks extends Connector
{
    /**
     * @var int
     */
    protected $item;

    protected $use_cache = false;

    public function getStockForWarehouse($warehouseId = 1, $optionId = 0)
    {
        return $this->initSingleItem('inventory/items/'.$this->item.'/stock', ['option_id' => $optionId, 'warehouse_id' => $warehouseId]);
    }

    /**
     * @param $contact_category
     * @return $this
     */
    public function setInventoryItem($item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * Create new data instance
     * @param $item
     * @return InventoryStock
     */
    public function initObject($item = [])
    {
        return new InventoryStock($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'inventory/items/'.$this->item.'/stock';
    }
}