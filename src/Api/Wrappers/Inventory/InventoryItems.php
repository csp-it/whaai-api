<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Inventory;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryItem;

class InventoryItems extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return InventoryItem
     */
    public function initObject($item = [])
    {
        return new InventoryItem($this, $item);
    }

    public function all()
    {
        $this->where('order_by', 'website_order');
        return parent::all();
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'inventory/items';
    }
}