<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Inventory;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryPurchaseOrder;

class InventoryPurchaseOrders extends Connector
{
    protected $use_cache = false;

    /**
     * Create new data instance
     * @param $item
     * @return InventoryPurchaseOrder
     */
    public function initObject($item = [])
    {
        return new InventoryPurchaseOrder($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'inventory/purchaseorders';
    }
}