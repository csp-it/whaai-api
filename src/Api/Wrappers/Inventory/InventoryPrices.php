<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Inventory;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryPrice;

class InventoryPrices extends Connector
{
    /**
     * @var int
     */
    protected $item;
    protected $use_cache = false;


    public function getPriceForQuantity($qty)
    {
        return $this->initSingleItem('inventory/items/'.$this->item.'/price', ['price_for_qty' => $qty]);
    }

    /**
     * @param $contact_category
     * @return $this
     */
    public function setInventoryItem($item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * Create new data instance
     * @param $item
     * @return InventoryPrice
     */
    public function initObject($item = [])
    {
        return new InventoryPrice($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'inventory/items/'.$this->item.'/price';
    }
}