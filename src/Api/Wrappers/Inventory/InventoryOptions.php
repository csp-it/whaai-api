<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Inventory;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryCombination;

class InventoryOptions extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return InventoryCombination
     */
    public function initObject($item = [])
    {
        return new InventoryCombination($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'inventory/items/option';
    }
}