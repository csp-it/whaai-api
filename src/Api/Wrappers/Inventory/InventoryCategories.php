<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Inventory;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryCategory;

class InventoryCategories extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return InventoryCategory
     */
    public function initObject($item = [])
    {
        return new InventoryCategory($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'inventory/categories';
    }
}