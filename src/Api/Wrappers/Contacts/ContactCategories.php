<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Contacts;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Contacts\ContactCategory;

class ContactCategories extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return ContactCategory
     */
    public function initObject($item = [])
    {
        return new ContactCategory($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'contacts/categories';
    }
}