<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Contacts;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Contacts\Contact;

class Contacts extends Connector
{
    /**
     * @var int
     */
    protected $contact_category;

    /**
     * @param $contact_category
     * @return $this
     */
    public function setContactCategory($contact_category)
    {
        $this->contact_category = $contact_category;
        return $this;
    }

    /**
     * Create new data instance
     * @param $item
     * @return Contact
     */
    public function initObject($item = [])
    {
        return new Contact($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'contacts/categories/'.$this->contact_category.'/contacts';
    }
}