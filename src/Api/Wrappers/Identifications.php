<?php

namespace Whaai\WhaaiApi\Api\Wrappers;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Country;

/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 7/13/2016
 * Time: 10:40 PM
 */
class Identifications extends Connector
{
    protected $index_paginated = false;

    public function entity($driver, $prefix, $id)
    {
        $response = $this->get($this->getPrefix(), [
            'driver' => $driver,
            'prefix' => $prefix,
            'id' => $id,
        ]);

        return $response;
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'identifications';
    }

    /**
     * Create new data instance
     * @param $item
     * @return Country
     */
    public function initObject($item = [])
    {
        return null;
    }
}