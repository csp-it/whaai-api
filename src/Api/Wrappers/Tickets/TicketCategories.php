<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Tickets;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryItem;
use Whaai\WhaaiApi\Api\Data\Tickets\TicketCategory;

class TicketCategories extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return InventoryItem
     */
    public function initObject($item = [])
    {
        return new TicketCategory($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'tickets/categories';
    }
}