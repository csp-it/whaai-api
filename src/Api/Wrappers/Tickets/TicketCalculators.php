<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Tickets;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Inventory\InventoryItem;
use Whaai\WhaaiApi\Api\Data\Tickets\TicketCalculator;

class TicketCalculators extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return InventoryItem
     */
    public function initObject($item = [])
    {
        return new TicketCalculator($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'tickets/calculator';
    }
}