<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Tickets;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Tickets\TicketActivity;

class TicketActivities extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return TicketActivity
     */
    public function initObject($item = [])
    {
        return new TicketActivity($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'tickets/activities';
    }
}