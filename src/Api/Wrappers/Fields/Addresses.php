<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Fields;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Fields\Address;

class Addresses extends Connector
{
    public function filter($item_type, $item_id, $field_id)
    {
        return $this->where('item_type', $item_type)
                ->where('item_id', $item_id)
                ->where('field_id', $field_id);
    }

    /**
     * Create new data instance
     * @param $item
     * @return Address
     */
    public function initObject($item = [])
    {
        return new Address($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'fields/addresses';
    }
}