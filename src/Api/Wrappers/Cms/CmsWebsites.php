<?php

namespace Whaai\WhaaiApi\Api\Wrappers\Cms;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Cms\CmsWebsite;

class CmsWebsites extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return CmsWebsite
     */
    public function initObject($item = [])
    {
        return new CmsWebsite($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'cms/website';
    }
}