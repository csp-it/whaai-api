<?php

namespace Whaai\WhaaiApi\Api\Wrappers;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Tax;

class Taxes extends Connector
{
    /**
     * Create new data instance
     * @param $item
     * @return Tax
     */
    public function initObject($item = [])
    {
        return new Tax($this, $item);
    }

    public function forContact($contact_id)
    {
        $tax = $this->get('taxes/for-contact/'.$contact_id);
        if (isset($tax['tax'])) {
            return $this->initObject($tax['tax']);
        }

        return null;
    }

    public function forAddress($address_id)
    {
        $tax = $this->initObject($this->get('taxes/for-address/'.$address_id, [
            'entity' => 'person',
        ]));

        if (isset($tax['tax'])) {
            return $this->initObject($tax['tax']);
        }

        return null;
    }

    /**
     * Prefix for all the api calls related to the taxes
     * @return string
     */
    public function getPrefix()
    {
        return 'taxes';
    }
}