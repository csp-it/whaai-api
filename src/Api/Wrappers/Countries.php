<?php

namespace Whaai\WhaaiApi\Api\Wrappers;

use Whaai\WhaaiApi\Api\Connector;
use Whaai\WhaaiApi\Api\Data\Country;

/**
 * Created by PhpStorm.
 * User: Nadin Yamaui
 * Date: 7/13/2016
 * Time: 10:40 PM
 */
class Countries extends Connector
{
    protected $index_paginated = false;

    public function getList()
    {
        return $this->all()->pluck('label', 'code2')->all();
    }

    public function getNationalityList()
    {
        return $this->all()->pluck('nationality', 'code2')->filter()->all();
    }

    /**
     * Create new data instance
     * @param $item
     * @return Country
     */
    public function initObject($item = [])
    {
        return new Country($this, $item);
    }

    /**
     * Prefix for all the api calls related to the contact categories
     * @return string
     */
    public function getPrefix()
    {
        return 'countries';
    }
}