<?php


namespace Whaai\WhaaiApi\Api;

use Whaai\WhaaiApi\Api\Data\BaseModel;
use Whaai\WhaaiApi\Api\Models\ApiRequestCache;
use Whaai\WhaaiApi\Exceptions\BadRequestException;
use Whaai\WhaaiApi\Exceptions\InternalServerException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Event\CompleteEvent;
use GuzzleHttp\Message\ResponseInterface;
use Serializable;

abstract class Connector implements Serializable
{
    public static $base_uri;
    protected $cache_duration;
    protected $use_cache = true;
    protected $encoded_credentials;

    /**
     * @var bool
     */
    protected $index_paginated = true;

    /**
     * @var array
     */
    protected $where_conditions = [];

    /**
     * @var string
     */
    protected $order_by = "";

    /**
     * @var Client
     */
    protected $http_client;

    /**
     * @var Collection
     */
    protected $collection;

    public function __construct($tenant_token, $user_token, $api_url, $cache_duration)
    {
        static::$base_uri = $api_url;
        $this->cache_duration = $cache_duration;
        $this->encoded_credentials = base64_encode($tenant_token . ':' . $user_token);
        $this->initHttpClient();
    }

    protected function initHttpClient()
    {
        $this->http_client = new Client([
            // Base URI is used with relative requests
            'base_url' => static::$base_uri,
            'defaults' => [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . $this->encoded_credentials,
                ],
            ],
        ]);

        if (app()->bound('debugbar')) {
            $this->http_client->getEmitter()->once('complete', function (CompleteEvent $event) {
                $debugBar = app()->make('debugbar');
                $debugBar['api_requests']->addRequest($event);
            });
        }
    }

    /**
     * Make a PUT requests to the API
     * @param string $url of the API
     * @param array $form_params form data
     * @return array
     * @throws InternalServerException
     * @throws BadRequestException
     */
    public function put($url, $form_params = [])
    {
        $form_params['_method'] = 'put';
        return $this->post($url, $form_params);
    }

    /**
     * Make a post requests to the API
     * @param string $url of the API
     * @param array $form_params form data
     * @return array
     * @throws InternalServerException
     * @throws BadRequestException
     */
    public function post($url, $form_params = [])
    {
        try {
            $response = $this->http_client->post($url, [
                'body' => $form_params,
            ]);
        } catch (ServerException $exception) {
            throw (new InternalServerException())
                ->setUrl($url)
                ->setResponse($exception->getResponse())
                ->setRequest($exception->getRequest())
                ->setMessage();
        } catch (ClientException $exception) {
            throw (new BadRequestException($exception))
                ->setResponse($exception->getResponse())
                ->setMessage();
        }

        return $response->json();
    }

    /**
     * Return a collection of items
     * @param int $limit
     * @return Collection|self
     */
    public function paginate($limit = 50)
    {
        return $this->getCollection()->paginate($limit);
    }

    protected function getCollection()
    {
        if (is_null($this->collection) || $this->collection->isEmpty()) {
            $params = [
                'page' => 1,
                'limit' => 9999999,
            ];
            $params = array_merge($params, $this->where_conditions);
            if ($this->order_by) {
                $params['order_by'] = $this->order_by;
            }
            $this->collection = $this->initCollection($this->getPrefix(), $params);
        }
        return $this->collection;
    }

    /**
     * Returns a collection of the Data class with the API response
     * @param $url
     * @param array $parameters
     * @return Collection
     */
    protected function initCollection($url, $parameters = [])
    {
        $items = $this->get($url, $parameters);
        $dataCollection = new Collection();

        if ($this->index_paginated) {
            foreach ($items['data'] as $item) {
                $dataCollection->push($this->initObject($item));
            }
        } else {
            $items = reset($items);
            if (is_array($items)) {
                foreach ($items as $item) {
                    $dataCollection->push($this->initObject($item));
                }
            }
        }

        return $dataCollection;
    }

    /**
     * Make a get requests to the API
     * @param string $url url of the API
     * @param array $parameters Query String parameters
     * @return array
     * @throws InternalServerException
     * @throws BadRequestException
     */
    public function get($url, $parameters = [])
    {
        $url = $url . '?' . http_build_query($parameters);
        $cache = $this->getCache($url);
        if ($cache) {
            return $cache->api_response;
        }

        return $this->getFromServer($url);
    }

    /**
     * Retrieve a request from the API cache
     * @param $url
     * @return null
     */
    protected function getCache($url)
    {
        if ($this->use_cache) {
            $cache = ApiRequestCache::findByUri($url);
            if ($cache->exists) {
                //how old is the cache?.
                $dateCheck = $cache->updated_at->addMinutes($this->cache_duration);

                if ($dateCheck->gt(new Carbon())) {
                    return $cache;
                }
            }
        }

        return null;
    }

    /**
     * Forward the call to the API server
     * @param $url
     * @return array
     * @throws InternalServerException
     * @throws BadRequestException
     */
    protected function getFromServer($url)
    {
        try {
            $response = $this->http_client->get($url);
        } catch (ServerException $exception) {
            throw (new InternalServerException())
                ->setUrl($url)
                ->setResponse($exception->getResponse())
                ->setRequest($exception->getRequest())
                ->setMessage();
        } catch (ClientException $exception) {
            throw (new BadRequestException($exception))
                ->setResponse($exception->getResponse())
                ->setMessage();
        }

        return $this->response($response, $url);
    }

    /**
     * @param ResponseInterface $response
     * @param string $url
     * @return array
     */
    protected function response(ResponseInterface $response, $url)
    {
        if (in_array($response->getStatusCode(), [200, 201])) {
            $this->cacheResponse($response, $url);
            return $response->json();
        }

        return [];
    }

    /**
     * Save the API response in the cache table
     * @param ResponseInterface $response
     * @param $url
     */
    protected function cacheResponse(ResponseInterface $response, $url)
    {
        $cache = ApiRequestCache::findByUri($url);
        $cache->api_response = $response->json();
        $cache->saveAndRemember();
    }

    public abstract function initObject($item = []);

    public abstract function getPrefix();

    /**
     * Return a collection of all items (max 9999)
     * @return Collection|self
     */
    public function all()
    {
        return $this->getCollection();
    }

    /**
     * Retrieve an object by ID
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->initSingleItem($this->getPrefix() . '/' . $id);
    }

    protected function initSingleItem($url, $parameters = [])
    {
        $response = $this->get($url, $parameters);
        $object = reset($response);

        return $this->initObject($object);
    }

    /**
     * @param $id
     * @return BaseModel
     */
    public function findOrNew($id)
    {
        try {
            if ($id > 0) {
                return $this->initSingleItem($this->getPrefix() . '/' . $id);
            }
            return $this->initObject();
        } catch (BadRequestException $exception) {
            return $this->initObject();
        }
    }

    public function findByUuid($uuid)
    {
        return $this->initSingleItem($this->getPrefix() . '/' . $uuid . '/uuid');
    }

    /**
     * Retrieve an object by ID
     * @param $id
     * @return mixed
     */
    public function findInCollection($id)
    {
        return $this->getCollection()->find($id);
    }

    public function where($column, $value)
    {
        $this->where_conditions[$column] = $value;
        return $this;
    }

    public function orderBy($column)
    {
        $this->order_by = $column;
        return $this;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->cache_duration,
            $this->encoded_credentials,
            $this->index_paginated,
            $this->where_conditions,
            $this->order_by,
            $this->collection,
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list($this->cache_duration,
            $this->encoded_credentials,
            $this->index_paginated,
            $this->where_conditions,
            $this->order_by,
            $this->collection) = unserialize($serialized);

        $this->http_client = $this->initHttpClient();
    }

    protected function registerEvents()
    {

    }
}