<?php

namespace Whaai\WhaaiApi\Api\Integrations\Debugbar;

use DebugBar\DataCollector\Renderable;
use DebugBar\DataCollector\TimeDataCollector;
use GuzzleHttp\Event\CompleteEvent;

class WhaaiCollector extends TimeDataCollector implements Renderable
{
    /**
     * @var float
     */
    protected $total_time;

    /**
     * @var int
     */
    protected $total_requests;

    /**
     * Create a WhaaiCollector
     */
    public function __construct()
    {
        $this->total_time = 0;
        $this->total_requests = 0;
        $this->collect_data = true;
    }

    public function addRequest(CompleteEvent $event)
    {
        $finished = microtime(true);
        $information = $event->getTransferInfo();
        $totalTime = array_get($information, 'total_time');
        $params = [
            'url' => array_get($information, 'url'),
            'method' => $event->getRequest()->getMethod(),
            'query_string' => json_encode($event->getRequest()->getQuery()->toArray()),
            'response_code' => array_get($information, 'http_code'),
            'connect_time' => $this->getDataFormatter()->formatDuration(array_get($information, 'connect_time')),
            'total_time' => $this->getDataFormatter()->formatDuration($totalTime),
            'size_download' => $this->getDataFormatter()->formatBytes(array_get($information, 'size_download')),
            'speed_download' => $this->getDataFormatter()->formatBytes(array_get($information, 'speed_download')),
            'speed_upload' => $this->getDataFormatter()->formatBytes(array_get($information, 'speed_upload')),
        ];

        $this->total_time += array_get($information, 'total_time');
        $this->total_requests++;

        $this->addMeasure($params['url'], $finished - $totalTime, $finished, $params);
    }

    /**
     * Called by the DebugBar when data needs to be collected
     *
     * @return array Collected data
     */
    public function collect()
    {
        $data = parent::collect();
        $data['total_time'] = $this->getDataFormatter()->formatDuration($this->total_time);
        $data['total_requests'] = $this->total_requests;

        return $data;
    }

    /**
     * Returns the unique name of the collector
     *
     * @return string
     */
    public function getName()
    {
        return "api_requests";
    }

    /**
     * {@inheritDoc}
     */
    public function getWidgets()
    {
        return [
            'api_requests' => [
                "icon" => "tasks",
                "widget" => "PhpDebugBar.Widgets.TimelineWidget",
                "map" => "api_requests",
                "default" => "{}",
            ],
            "api_requests_total_time" => [
                "icon" => "clock-o",
                "tooltip" => "API Total Time",
                "map" => "api_requests.total_time",
                "default" => ""
            ],
            'api_requests:badge' => [
                'map' => 'api_requests.total_requests',
                'default' => 0
            ]
        ];
    }
}