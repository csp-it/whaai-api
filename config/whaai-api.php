<?php

return array(
    'website_id' => env('WEBSITE_ID', 1),
    'tenant_token' => env('TENANT_TOKEN'),
    'user_token' => env('USER_TOKEN'),
    'api_url' => env('API_URL', 'https://api.whaai.com/api/'),
    'cache_duration' => env('CACHE_DURATION', '30'),
    'disable_files_cache' => env('DISABLE_FILES_CACHE', false),
    'path_for_synced_files' => public_path('uploads'),
    'prefix_url_for_synced_files' => 'uploads',
    'files' => [
        'synced_item_types' => [],
    ],
    'backup' => [
        'enabled' => env('BACKUP_ENABLED', false),
        'disk' => env('BACKUP_DISK_NAME'),
        'mysqldump' => env('MYSQLDUMPPATH', 'mysqldump'),
        'hipchat_notification' => env('BACKUP_HIPCHAT_ENABLED', true),
        'hipchat_room' => env('BACKUP_HIPCHAT_ROOM', true),
        'hipchat_api_token' => env('BACKUP_HIPCHAT_API_TOKEN', true),
    ],
);
