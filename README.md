# Whaai API #

Installation of Whaai API Package on Laravel project

## Install Laravel Project ##

First, download the Laravel installer using Composer:
```bash
composer global require "laravel/installer"
```
Once installed, the laravel new command will create a fresh Laravel installation in the directory you specify. For instance, `laravel new blog` will create a directory named blog containing a fresh Laravel installation with all of Laravel's dependencies already installed:
```bash
laravel new blog
```
## Configure Whaai API Package ##

Add Whaai API Package Service Provider on config/app.php
```php
Whaai\WhaaiApi\WhaaiApiServiceProvider::class,
```

Add Application Service Provider on config/app.php
```php
App\Providers\ComposerServiceProvider::class,
```

Run composer install
```bash
composer install
```

Run composer update
```bash
composer update
```

Run Migrations
```bash
php artisan migrate
```