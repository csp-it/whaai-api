<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiRequestsCacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_requests_cache', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uri');
            $table->longText('api_response');
            $table->timestamps();

            $table->unique(['uri']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_requests_cache');
    }
}
