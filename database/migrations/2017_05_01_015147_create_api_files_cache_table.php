<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiFilesCacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_files_cache', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_uuid', 36);
            $table->string('local_file_path');
            $table->string('extension');
            $table->string('public_link')->nullable();
            $table->dateTime('last_update_on_server');
            $table->timestamps();
            $table->unique('file_uuid');
            $table->string('filename');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_files_cache');
    }
}
